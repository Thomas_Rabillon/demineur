import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * La classe Plateau représente un plateau de démineur
 */
public class Plateau {
    private int nbLignes;
    private int nbColonnes;
    private int nbBombes;
    private int pourcentageDeBombes;
    private List<List<CaseIntelligente>> lePlateau;

    /**
     * Permet de créer un nouveau plateau de jeu
     * @param nbLignes La largeur du plateau
     * @param nbColonnes La longueur du plateau
     * @param pourcentage Le nombre de bombe du plateau
     */
    public Plateau(int nbLignes, int nbColonnes, int pourcentage) {
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.pourcentageDeBombes = pourcentage;
        this.nbBombes = this.nbColonnes*this.nbLignes*(this.pourcentageDeBombes/100);
        this.lePlateau = new ArrayList<>();

        this.creerLesCasesVides();
        this.rendLesCasesIntelligentes();
    }

    /**
     * Permet de creer les cases vides du plateau
     */
    private void creerLesCasesVides() {
        for (int i = 0; i < this.nbLignes; i++) {
            lePlateau.add(new ArrayList<>());
            for (int j = 0; j < this.nbColonnes; j++) {
                lePlateau.get(i).add(new CaseIntelligente());
            }
        }
    }

    /**
     * Permet de rendre les cases intelligentes, c'est-à-dire qu'elles connaissent leurs voisines
     */
    private void rendLesCasesIntelligentes() {
        for (int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                for (int x = i-1; x <= i+1; x++) {
                    for (int y = j-1; y <= j+1; y++) {
                        if (x >= 0 && y >= 0 && x < this.nbLignes && y < this.nbColonnes && !(x==0 && y==0)) {
                            this.lePlateau.get(i).get(j).ajouteVoisine(this.getCase(x, y));
                        }
                    }
                }
            }
        }
    }

    /**
     * Permet de poser les bombes aléatoirement
     */
    protected void poseDesBombesAleatoirement() {
        Random generateur = new Random();
        for (int x = 0; x < this.getNbLignes(); x++){
            for (int y = 0; y < this.getNbColonnes(); y++){
                if (generateur.nextInt(100)+1 < this.pourcentageDeBombes){
                    this.poseBombe(x, y);
                    this.nbBombes = this.nbBombes + 1;
                }
            }
        }
    }

    /**
     * Permet d'obtenir le nombre de ligne du plateau
     * @return Le nombre de ligne du plateau
     */
    public int getNbLignes() { return nbLignes; }

    /**
     * Permet d'obtenir le nombre de colonne du plateau
     * @return Le nombre de colonne du plateau
     */
    public int getNbColonnes() { return nbColonnes; }

    /**
     * Permet de savoir combien de bombe il y a sur le plateau
     * @return Le nombre de bombe sur le plateau
     */
    public int getNbTotalBombes() { return this.nbBombes; }

    /**
     * Permet d'obtenir la case se trouvant au coordonnée (x,y)
     * @param x L'axe x du plateau
     * @param y L'axe y du plateau
     * @return La case correspondante au coordonnée sur le plateau
     */
    public CaseIntelligente getCase(int numLigne, int numColonne) { return this.lePlateau.get(numLigne).get(numColonne); }

    /**
     * Permet d'obtenir le nombre de cases marquées
     * @return Le nombre de cases marquées
     */
    public int getNbCasesMarquees() {
        int nbCasesMarquees = 0;
        for (int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                if (this.getCase(i, j).estMarquee()) {
                    nbCasesMarquees++;
                }
            }
        }
        return nbCasesMarquees;
    }

    public int getNbCasesRevelees() {
        int nbCasesRevelees = 0;
        for (int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                if (this.getCase(i, j).estDecouverte()) {
                    nbCasesRevelees++;
                }
            }
        }
        return nbCasesRevelees;
    }

    /**
     * Permet de poser une bombe sur la case aux coordonnées (x,y)
     * @param x L'abscisse de la case
     * @param y L'ordonnée de la case
     */
    public void poseBombe(int x, int y) { this.lePlateau.get(x).get(y).poseBombe(); }

    /**
     * Permet de reset toutes les cases du plateau
     */
    public void reset() {
        for (List<CaseIntelligente> ligneCases : this.lePlateau) {
            for (CaseIntelligente c : ligneCases) {
                c.reset();
                this.nbBombes = 0;
            }
        }
    }
}
