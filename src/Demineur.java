import java.util.Scanner;

public class Demineur extends Plateau {
    private boolean gameOver;
    private int score;

    /**
     * Permet de créer un nouveau démineur
     * @param nbLignes Le nombre de lignes du démineur
     * @param nbColonnes Le nombre de colonnes du démineur
     * @param pourcentage Le pourcentage de bombes du démineur
     */
    public Demineur(int nbLignes, int nbColonnes, int pourcentage) {
        super(nbLignes, nbColonnes, pourcentage);
        this.gameOver = false;
        this.score = 0;
    }

    /**
     * Permet d'obtenir le score
     * @return Le score
     */
    public int getScore() { return this.score; }

    /**
     * Permet de réveler une case
     * @param x L'abcisse de la case
     * @param y L'ordonée de la case
     */
    public void reveler(int x, int y) {
        this.gameOver = this.getCase(x, y).reveler();
        this.score++; 
    }

    /**
     * Premet de marquer une case
     * @param x L'abcisse de la case
     * @param y L'ordonée de la case
     */
    public void marquer(int x, int y)  { this.getCase(x, y).marquer(); }

    /**
     * Permet de savoir si la partie est gagnée
     * @return true si la partie est gagnée et false sinon
     */
    public boolean estGagnee() {
        for (int i = 0; i < super.getNbLignes(); i++) {
            for (int j = 0; j < super.getNbColonnes(); j++) {
                if (this.getCase(i, j).contientUneBombe() && !(this.getCase(i, j).estMarquee())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Premet de savoir si la partie est perdue
     * @return true si la partie est perdue et false sinon
     */
    public boolean estPerdue() {
        return this.gameOver;
    }

    /**
     * Permet de reset toute la partie
     */
    public void reset() {
        super.reset();
        this.gameOver = false;
        this.score = 0;
    }
    
    public void affiche(){
        System.out.println("JEU DU DEMINEUR");
        // affichage de la bordure supérieure
        System.out.print("  ");
        for (int j=0; j<this.getNbColonnes(); j++){
            System.out.print("  "+j+" ");
        }
        System.out.print(" \n");
        
        // affichage des numéros de ligne + cases
        System.out.print("  ┌");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┬");
        }
        System.out.println("───┐");
        
        // affichage des numéros de ligne + cases
        for (int i = 0; i<this.getNbLignes(); i++){
            System.out.print(i+" ");
            for (int j=0; j<this.getNbColonnes(); j++){
                System.out.print("│ "+this.getCase(i, j).toString() + " ");
            }
            System.out.print("│\n");
            
            if (i!=this.getNbLignes()-1){
                // ligne milieu
                System.out.print("  ├");
                for (int j=0; j<this.getNbColonnes()-1; j++){
                        System.out.print("───┼");
                }
                System.out.println("───┤");
            }
        }

        // affichage de la bordure inférieure
        System.out.print("  └");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┴");
        }
        System.out.println("───┘");
        
        // affichage des infos 
        System.out.println("Nombres de bombes à trouver : " + this.getNbTotalBombes());
        System.out.println("Nombres de cases marquées : " + this.getNbCasesMarquees());
        System.out.println("Score : " + this.getScore());
    }

    
    public void nouvellePartie(){
        this.reset();
        this.poseDesBombesAleatoirement();
        this.affiche();
        Scanner scan = new Scanner(System.in).useDelimiter("\n");

        while (!this.estPerdue() || this.estGagnee()){
            System.out.println("Entrer une instruction de la forme R 3 2 ou M 3 2\npour Révéler/Marquer la case à la ligne 3 et à la colonne 2");
            String [] s = scan.nextLine().split(" ");
            String action = s[0];
            int x = Integer.valueOf(s[1]);
            int y = Integer.valueOf(s[2]);
            if (action.equals("M") || action.equals("m"))
                this.marquer(x, y);
            else if (action.equals("R") || action.equals("r"))
                this.reveler(x, y);
            this.affiche();
        }
        if (this.gameOver){
            System.out.println("Oh !!! Vous avez perdu !");
        }
        else{
            System.out.println("Bravo !! Vous avez gagné !");
        }
    }
}
