import java.util.ArrayList;
import java.util.List;

public class CaseIntelligente extends Case {
    private List<Case> lesVoisines;

    /**
     * Permet de créer une nouvelle case intélligente
     */
    public CaseIntelligente() {
        super();
        this.lesVoisines = new ArrayList<>();
    }

    /**
     * Permet d'ajouter une case dans les cases voisines de cette case
     * @param uneCase Une case
     */
    public void ajouteVoisine(Case uneCase) {
        this.lesVoisines.add(uneCase);
    }

    /**
     * Permet de savoir combien il y a de bombes parmis les voisines de cette case
     * @return Le nombre de bombes voisines
     */
    public int nombreBombesVoisines() {
        int nbBombes = 0;
        for (Case c : this.lesVoisines) {
            if (c.contientUneBombe()) {
                nbBombes++;
            }
        }
        return nbBombes;
    }

    @Override
    public String toString() {
        if (super.estMarquee()) {
            return "?";
        } else if (super.estDecouverte()) {
            if (super.contientUneBombe()) {
                return "@";
            } else {
                return "" + this.nombreBombesVoisines();
            }
        } else {
            return " ";
        }
    }
}
