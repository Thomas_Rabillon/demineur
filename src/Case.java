/**
 * La classe Case représente une case de démineur
 */
public class Case {
    private boolean contientUneBombe;
    private boolean estDecouverte;
    private boolean estMarquee;

    /**
     * Permet de créer une nouvelle case
     */
    public Case() {
        this.contientUneBombe = false;
        this.estDecouverte = false;
        this.estMarquee = false;
    }

    /**
     * Permet de reset la case
     */
    public void reset() {
        this.contientUneBombe = false;
        this.estDecouverte = false;
        this.estMarquee = false;
    }

    /**
     * Permet de poser une bombe sur cette case
     */
    public void poseBombe() { this.contientUneBombe = true; }

    /**
     * Permet de savoir si une case est une bombe
     * @return true si la case est une bombe et false sinon
     */
    public boolean contientUneBombe() { return this.contientUneBombe; }

    /**
     * Permet de savoir si une case est revelée
     * @return true si la case est revelée et false sinon
     */
    public boolean estDecouverte() { return this.estDecouverte; }

    /**
     * Permet de savoir si une case est marquée
     * @return true si la case est marquée et false sinon
     */
    public boolean estMarquee() { return this.estMarquee; }

    /**
     * Permet de reveler la case
     * @return false si tout va bien et true si il y avait une bombe
     */
    public boolean reveler() { 
        if (this.estDecouverte || this.estMarquee) {
            return false;
        }
        this.estDecouverte = true;
        return this.contientUneBombe(); 
    }

    /**
     * Permet de marquer une case
     */
    public void marquer() {
        if (this.estMarquee) {
            this.estMarquee = false; 
        } else {
            this.estMarquee = true; 
        }
    }
}